# Dicecheck
A simple script in Python that generates random numbers in a way that tries to simulate the throw of a dice, or multiple dices in one shot.  
  
---
---
## Usage and scope
This script can be called from the command line and it will take two positional parameters:

- The first positional parameter determines the type of dice used (defaults to 6 if not specified).
- The second positional parameter will determine the number of launches (it defaults to 1 if not specified). If this option is used to generate more than one number the script will show the total sum of the launches in the output.

### Example:
Calling the script with the command `dicecheck 8 5` will generate 5 random numbers between 1 and 8 and also print the total sum of all those numbers, so that the output will look something like this:  
  
```
3
6
8
8
4
sum: 29
```

This can be particularly useful to roleplay gamers, since they often need to throw multiple dices in a single check. When the dices are many this can be very cumbersome, slowing down the game. 
This script will handle those situations pretty well, resolving the check in a blink of an eye.  
  
---
## Installation and dependencies
Just put the script in a folder that is in your `$PATH` variable, or call it from the folder in which it resides.   
You have to have the Python interpreter insalled on your system.
